//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Drummer on 2022. 04. 30..
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
